<?php

use App\Movie;
use Illuminate\Database\Seeder;

class MoviesSeederTableSeeder extends Seeder
{
    public function run()
    {
        Movie::unguard();
        Movie::create(['title' => 'タイタニック']);
        Movie::create(['title' => 'アバター']);
    }
}
