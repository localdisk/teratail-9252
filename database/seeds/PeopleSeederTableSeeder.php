<?php

use App\Person;
use Illuminate\Database\Seeder;


class PeopleSeederTableSeeder extends Seeder
{
    public function run()
    {
        Person::unguard();
        Person::create(['name' => 'ジェームズ・キャメロン']);
        Person::create(['name' => 'レオナルド・デカプリオ ']);
    }
}
