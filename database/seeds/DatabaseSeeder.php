<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * データベース初期値設定実行
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(MoviesSeederTableSeeder::class);
        $this->call(PeopleSeederTableSeeder::class);
        $this->call(MoviePersonSeederTableSeeder::class);
    }

}
