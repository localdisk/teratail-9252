<?php

use Illuminate\Database\Seeder;

class MoviePersonSeederTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('movie_person')->insert([
            [
                'movie_id'  => 1,
                'person_id' => 1,
                'role'      => '監督'
            ],
            [
                'movie_id'  => 1,
                'person_id' => 2,
                'role'      => '主演'
            ],
        ]);
    }

}
