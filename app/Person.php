<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Person
 *
 * @property integer $id 
 * @property string $name 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Person whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Person whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Person whereUpdatedAt($value)
 */
class Person extends Model {

     public function movies()
     {
         return $this->belongsToMany(Movie::class);
     }

}
