<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Movie
 *
 * @property integer $id 
 * @property string $title 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Movie whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Movie whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Movie whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Movie whereUpdatedAt($value)
 */
class Movie extends Model {

     public function people()
     {
         return $this->belongsToMany(Person::class)->withPivot('role');
     }

}
