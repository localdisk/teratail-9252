<!DOCTYPE HTML>
<html lang="ja-JP">
    <head>
        <title>Movie</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>Movie</h1>
            </div>
            <div>
                <h2>{{ $movie->title }}</h2>
                <h5>company:</h5>
                <ul>
                    @foreach($movie->people as $person)
                    <li>{{$person->pivot->role}} : {{ $person->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>